-----------
--QUICKSORT
-----------
--the function "qsort" "has type of" "any type that can be ordered" ('a' is a type variable)
--"Ord" is a typeclass; types belonging to "Ord" possess definitions for the comparison operators
--qsort maps a list of some Orderable type 'a' to a list of the same Orderable type 'a'
qsort :: Ord a => [a] -> [a] 
qsort [] = [] --qsort applied to an empty list returns an empty list
--"(p:xs)" mean the list with head 'p' and tail "xs"
qsort (p:xs) = (qsort lesser) ++ [p] ++ (qsort greater) --"++" is a list concatenation operator 
   where
      lesser = filter (<p) xs  --filter is a function with two parameters (a predicate and a list)
      greater = filter (>=p) xs --this filter returns a list with all elements from 'xs' >= 'p'



-----------
--MERGESORT
-----------
--function "msort" maps a list of any orderable data type to a list of the same orderable 
-- data type
msort :: Ord a => [a] -> [a]
msort [] = [] --msort on empty list returns empty list
msort [x] = [x] --msort on singleton returns singleton
msort xs = merge (msort left) (msort right) --split xs, msort halves, then merge sorted halves
  where
    -- splitAt returns tuple of two lists; 1st arg is where to split, 2nd arg is a list
    (left, right) = splitAt ((length xs) `div` 2) xs --div performs integer division 

merge :: Ord a => [a] -> [a] -> [a] --first two '[a]'s are parameters, third is return type
merge [] ys = ys
merge xs [] = xs --an empty list merged with a non-empty list returns the non-empty list
merge (x:xs) (y:ys)
  | x <= y    = x : merge xs (y:ys) --if x not greater than y, place it in front
  | otherwise = y : merge (x:xs) ys --otherwise place y in front



aList = [4,65,2,-31,0,99,2,83,782,1]



main = do
   putStr "List to sort: " 
   print aList
   putStr "quicksort: " 
   print (qsort aList)
   putStr "mergesort: " 
   print (msort aList) 
