#include <stdio.h>
#include <stdlib.h>


//FUNCTION PROTOTYPES
void qsort2(int *a, int n);
void msort(int *a, int n);
void merge(int *lo, int *hi);
void show(int *lo, int *hi);


int main()
{
    //init lists to be sorted
    int a[] = {4,65,2,-31,0,99,2,83,782,1};
    int b[] = {4,65,2,-31,0,99,2,83,782,1};
    //get size of list
    int n = sizeof(a) / sizeof(a[0]);

    qsort2(a, n);
    printf("quicksort: ");
    show( a, a+(n-1) );

    msort(b, n);
    printf("\nmergesort: ");
    show( b, b+(n-1) );

    printf("\nDONE\n");
    return 0;
}



/**
 * @param a : array to be sorted.
 * @param n : length of array.
**/
void msort(int *a, int n)
{
    if(n == 1) { return; }
    //recursive call on left half
    msort(a, n/2);
    //recursive call on right half
    msort(a + n/2, n - n/2);
    //merge sorted halves
    merge(a, a + n-1);
}

/**
 * @param lo : start of array to be sorted
 * @param hi : end of array to be sorted
**/
void merge(int *lo, int *hi)
{
    int len = (hi-lo) + 1;
    //admin array to hold sorted elements
    int *temp = (int*) malloc( len * sizeof(int) );
    //iterates over admin array
    int *i = temp;
    //iterates over left array
    int *left = lo;
    //beginning of right array
    int *mid = lo + len/2;
    //iterates over right array
    int *right = mid;
    //copy to temp in sorted order
    while(left < mid && right <= hi)
    {
        if(*left <= *right)
        {
            *i = *left;
            left++;
        }
        else
        {
            *i = *right;
            right++;
        }
        i++;
    }
    //copy over remainder of right array
    if(left == mid && right <= hi)
    {
        for(; right <= hi; right++)
        {
            *i = *right;
            i++;
        }
    }
    //copy over remainder of left array
    else
    {
        for(; left < mid; left++)
        {
            *i = *left;
            i++;
        }
    }
    //copy merged array back to original location
    i = temp;
    left = lo;

    while(left <= hi)
    {
        *left = *i;
        i++;
        left++;
    }
    free(temp);
}

/**
 * @param a : array to be sorted.
 * @param n : length of array.
**/
void qsort2(int *a, int n)
{
    int pvtIdx = n/2;
    int pvtVal = *(a + n/2);
    int left = 0;
    int right = n-1;
    int temp;

    while(left < right)
    {
        //find swappable element from left
        while(a[left] <= pvtVal && left < pvtIdx)
        {
            left++;
        }
        //find swappable element from right
        while(a[right] >= pvtVal && right > pvtIdx)
        {
            right--;
        }
        //swap
        if(left < right)
        {
            temp = a[left];
            a[left] = a[right];
            a[right] = temp;
            //follow moving pivot
            if(left == pvtIdx) { pvtIdx = right; }
            else if( right == pvtIdx ) { pvtIdx = left; }
        }
    }
    //sort elements left of pivot
    if(pvtIdx > 1) { qsort2(a, pvtIdx); }
    //sort elements right of pivot
    if(pvtIdx < n-2) { qsort2(a+pvtIdx+1, n-pvtIdx-1); }
}

/**
 * Prints to standard output contents of array beginning at lo and ending at hi.
**/
void show(int *lo, int *hi)
{
  int *i = lo;

  while(i <= hi)
  {
    printf("%d ", *i);
    i++;
  }
}
